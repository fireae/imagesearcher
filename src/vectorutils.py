import math, random, numpy

MIN_DIFF = 1

class Point(object):
    def __init__(self, point):
        self.point = point
        self.cluster = None
        
    def assignToCluster(self, cluster):
        self.cluster = cluster
        
def kmeans(data, k, iterNum=20):
    centers, points = initCentroids(data, k)
    centers, points = cluster(points, centers, 100)
    diff = updatedVariance(points)
    cRound = 0
    while diff > MIN_DIFF and cRound < iterNum :
        cRound +=1
        centers, points = cluster(points, centers, 100)
        diff = updatedVariance(points)
    return ([p.point for p in centers], diff)

def vq(features, code_book):
    ret = [0 for feature in features]
    for featureNdx in range(len(features)):
        feature = features[featureNdx]
        minDist = sum((feature - code_book[0])**2)
        for idx in range(1, len(code_book)):
            dist = sum((feature - code_book[idx])**2)
            if dist < minDist:
                minDist = dist
                ret[featureNdx] = idx
    return ret

def initCentroids(points, k):
    temp = []
    for i in range(k):
        temp.append(random.choice(points))
    centers = [Point(p) for p in temp]
    points = [Point(p) for p in points]
    for i in range(len(points)):
        points[i].assignToCluster(centers[i * k/len(points)])
    return (centers, points)

def cluster(points, centers, radius):
    centers = updatedCenters(points, centers)
    for p in points:
        minDist = radius
        for center in centers:
            dist = math.sqrt( sum( (center.point - p.point)**2 ) )
            if dist < minDist:
                minDist = dist
                p.assignToCluster(center)
    return (centers, points)

def updatedVariance(points):
    diff = 0.0
    for p in points:
        if p.cluster != None:
            diff += sum((p.point - p.cluster.point)**2)
    return math.sqrt(diff)/2

def updatedCenters(points, centers):
    for center in centers:
        temp = []
        for p in points:
            if p.cluster == center:
                temp.append(p.point)
        center.point = normalizedCenter(temp)
    return centers

def normalizedCenter(points):  
    point = numpy.mean(points, 0)
    return point# / math.sqrt(sum(point**2))