import pickle, math
from google.appengine.ext import ndb

class ImListEntry(ndb.Model):
    filename = ndb.StringProperty(indexed=True)
    
class ImWord(ndb.Model):
    imid = ndb.StringProperty(indexed=True)
    wordid = ndb.StringProperty(indexed=True)
    vocname = ndb.StringProperty(indexed=False)
    
class ImHistogram(ndb.Model):
    imid = ndb.StringProperty(indexed=True)
    histogram = ndb.StringProperty(indexed=False)
    vocname = ndb.StringProperty(indexed=False)
    
class Indexer(object):
    
    def __init__(self, db, voc):
        self.voc = voc
        
    def db_commit(self):
        pass
        
    def create_tables(self):
        pass
        
    def add_to_index(self, imname,  descr):
        """ Take an image with feature descriptors,
        project on vocabulary and add to database. """
        
        if self.is_indexed(imname):return
        #get the imid
        cImid = self.get_id(imname)
        #get the words
        imwords = self.voc.project(descr)
        nbr_words = imwords.shape[0]
        
        #link each word to image
        for i in range(nbr_words):
            cWord = imwords[i]
            imword = ImWord(imid=cImid, word=cWord, vocname=self.voc.name)
            imword.put()
        #store word histogram for image
        #use pickle to encode Numpy array as strings
        histo = ImHistogram(imid=cImid, histrogram=pickle.dumps(imwords), vocname=self.voc.name)
        histo.put()
            
    def is_indexed(self, imname):
        im = ImListEntry.query(ImListEntry.filename==imname).get()
        return im != None
    
    def get_id(self,imname):
        """ Get an entry id and add it if not present. """
        imEntry = ImListEntry.get_or_insert(ImListEntry==imname)
        return imEntry.index
    
class Searcher(object):
    
    def __init__(self, db, voc):
        self.voc = voc
        
    def __del__(self):
        self.con.close()
        
    def candidates_from_word(self, imword):
        """ Get list of images containing imwords. """
        ims = ImWord.query(ImWord.wordid==imword).fetchall()
        return [i[0].imid for i in ims]
    
    def candidates_from_histogram(self, imwords):
        """ Get list of images with similar words. """
        #get word ids
        words = imwords.nonzero()[0]
        #find candidates
        candidates = []
        for word in words:
            c = self.candidates_from_word(word)
            candidates += c
        
        #take all unique words amd reverse sort on occurence
        tmp = [(w, candidates.count(w)) for w in set(candidates)]
        tmp.sort(cmp=lambda x,y:cmp(x[1],y[1]))
        tmp.reverse()
        #return sorted list, best matches first
        return [w[0] for w in tmp]
    
    def get_imhistogram(self, imname):
        """ Return the word histogram for an image. """
        im_id = ImListEntry.query(ImListEntry.filename==imname).get()# self.con.execute("select rowid from imlist where filename='%s'" %imname).fetchone()
        s = ImHistogram.get_by_id(im_id.index)
        #use pickle to decode Numpy arrays from string
        return pickle.loads(str(s[0]))
    
    def query(self, imname):
        """ Find a list of matching images for imname. """
        
        h = self.get_imhistogram(imname)
        candidates = self.candidates_from_histogram(h)
        matchscores = []
        for imid in candidates:
            #get the name
            cand = ImListEntry.get_by_id(imid)
            cand_h = self.get_imhistogram(cand.filename)
            try:
                cand_dist = math.sqrt( sum( (h - cand_h)**2 ) ) #use L2 distance
                matchscores.append((cand_dist, imid))
            except ValueError:
                pass
        #return a sorted list of distances and database ids
        matchscores.sort()
        return matchscores