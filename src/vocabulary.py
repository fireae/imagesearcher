import sift
import numpy, math
import scipy.cluster.vq as vq

class Vocabulary(object):
    
    def __init__(self, name):
        self.name = name
        self.voc = []
        self.idf = []
        self.trainingdata = []
        self.nbr_words = 0
        
    def train(self, featurefiles, k=100, subsampling=10):
        """ Train a vocabulary from features in files listed in featurefiles using k-means with k number of words.
        Subsampling of training data can be used for speedup. """
        
        featurefiles = featurefiles[0::5] + featurefiles[1::10]
        nbr_images = len(featurefiles)
        #read the features from file
        descr = []
        descr.append(sift.read_features_from_file(featurefiles[0])[1])
        descriptors = descr[0] #stack all features for k-means
        for i in range(1, nbr_images):
            descr.append(sift.read_features_from_file(featurefiles[i])[1])
            descriptors = numpy.vstack((descriptors, descr[i]))
            
        #k-means: last number determines number of runs
        self.voc, distorsion = vq.kmeans(descriptors[::subsampling,:], k, 1)
        self.nbr_words = self.voc.shape[0]
        
        #go through all training images and project on vocabulary
        imwords = numpy.zeros((nbr_images, self.nbr_words))
        for i in range(nbr_images):
            imwords[i] = self.project(descr[i])
            
        nbr_occurences = sum( (imwords > 0) * 1,0)
        self.idf = [math.log( (1.0*nbr_images) / (1.0*freq+1)) for freq in nbr_occurences]
        self.trainingdata = featurefiles
        
    def project(self, descriptors):
        """ Project descriptors on the vocabulary
        to create a histogram of words. """
        
        #histogram of image words
        imhist = numpy.zeros((self.nbr_words))
        words, distance = vq.vq(descriptors, self.voc)
        for w in words:
            imhist[w] += 1
        return imhist
    
#Create vocabulary and save
def createVoc(imlist, vocPath, k=1000):
    nbr_images = len(imlist)
    featlist = [ imlist[i][:-3] + 'sift' for i in range(nbr_images) ]
    voc = Vocabulary('test')
    voc.train(featlist)
    #save
    with open(vocPath, 'wb') as f:
        import pickle
        pickle.dump(voc, f)
        f.close()
        
def openVoc(vocPath):
    with open(vocPath, 'rb') as f:
        import pickle
        ret = pickle.load(f)
        f.close()
        return ret
