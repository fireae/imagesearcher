import os, math, numpy as np, ImageFilter, Image
from cython import *
from chistogram import *
'''Implementation of SIFT'''

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "siftify", os.sep, "data", os.sep)

class Sift(object):
    def __init__(self, sigma, numLevels = 1):
        self.sigma = sigma
        self.numOctaves = -1
        self.numLevels = numLevels
    
    def DoG(self, image):
        self.numOctaves = int(math.log(min(image.size[0], image.size[1])))
        curImg = image.filter(ImageFilter.GaussianBlur(0.5))
        DoG = []
        cdef int levels, level, octave
        cdef float scale
        for octave in range(self.numOctaves):
            tempImg = curImg.copy()
            diffs = []
            levels = self.numLevels
            for level in range(-1, levels + 2):
                scale = self.sigma * math.pow(2, octave + level/levels)
                nextTemp= tempImg.filter(ImageFilter.GaussianBlur(scale))
                diffs.append(list(Image.blend(tempImg, Image.eval(nextTemp, lambda x:x/2), 2.0).getdata()))
                tempImg = nextTemp
            DoG.append(diffs) 
        return DoG
    
    def getExtrema(self, image):
        '''get candidate interesting points'''
        DoG = self.DoG(image)
        ret = []
        cdef int x, y, octave, level, isValid, pixVal, maxX, maxY
        maxX = image.size[0]
        maxY = image.size[1]
        for x in range(1, maxX - 1):
            for y in range(1, maxY - 1):
                isValid = 1
                for level in range(self.numLevels):
                    pixVal = DoG[0][0][x + y*maxX]
                    coords = self.genNeghbors(x, y)
                    pixels = [DoG[0][level][xy[0] + xy[1]*maxX] for xy in zip(coords[0], coords[1])]
                    if not self.isExtremum(pixVal, pixels):
                        isValid = 0
                        break
                if isValid:
                    for octave in range(1, self.numOctaves):
                        for level in range(self.numLevels):
                            pixVal = DoG[0][0][x + y*maxX]
                            coords = self.genNeghbors(x, y)
                            pixels = [DoG[octave][level][xy[0] + xy[1]*maxX] for xy in zip(coords[0], coords[1])]
                            if not self.isExtremum(pixVal, pixels):
                                isValid = 0
                                break
                if isValid:ret.append([x,y])
        return ret
    
    def isExtremum(self, point, neighbors):
        '''Detect if max or min'''
        def up(int p1, int p2):
            return p1 > p2
        def down(int p1, int p2):
            return p1 < p2
        comp = {1:up, 0:down}
        dominates = comp[up(point, neighbors[0])]
        cdef int i
        for i in range(len(neighbors)):
            if not dominates(point, neighbors[i]):
                return False
        return True
    
    def genNeghbors(self, xPos, yPos, considerSelf = False):
        '''
        Returns a list of neghboring pixels for (x, y). 
        param considerSelf consideres pixel (x,y) as a valid neighbor of itself.
        '''
        xRet = []
        yRet = []
        cdef x, y
        for x in range(xPos-1, xPos+2):
            for y in filter(lambda yVal: considerSelf or yVal != yPos or x != xPos, range(yPos-1, yPos+2)):
                xRet.append(x)
                yRet.append(y)
        return [xRet, yRet] 
    
    def gradientMagnitude(self, im, int pointX, int pointY, int totX, int totY, int parentDefault=1):
        '''Computes gradient magnitude for a given pixel in the image.'''
        cdef int temp1, temp2
        temp1 = im[(pointX+1) + (totX * pointY)] - im[(pointX-1) + (totX * pointY)]
        temp2 = im[pointX + totX * (pointY+1)] - im[pointX + totX * (pointY-1)]
        return math.sqrt((temp1 * temp1) + (temp2 * temp2)) * math.sqrt(parentDefault)
    
    def gradientOrientation(self, im, int pointX, int pointY, int totX, int totY):
        '''Computes gradient orientation for a given pixel in the image.'''
        cdef int ttemp1, ttemp2
        ttemp1 = im[pointX + totX * (pointY+1)] - im[pointX + totX * (pointY-1)]
        ttemp2 = im[(pointX+1) + (totX * pointY)] - im[(pointX-1) + (totX * pointY)]
        return math.atan2(ttemp1, ttemp2)
    
    def genHistogram(self, im, int pointX, int pointY, int totX, int totY, int numBins, float parentOrientation, int filterHighest):
        '''Generates orientation histogram for a given point.'''
        histogram = Histogram(self.sigma, numBins)
        cdef float angle
        angle = abs(parentOrientation - self.gradientOrientation(im, pointX, pointY, totX, totY))
        histogram.addToBins(angle, self.gradientMagnitude(im, pointX, pointY, totX, totY))
        return histogram.getHighestBins(filterHighest)
    
    def getDescriptors(self, image):
        '''Generates image descriptors.'''
        extrema = self.getExtrema(image)
        descriptors = []
        imData = list(image.getdata())
        cdef totX, totY
        totX = image.size[0]
        totY = image.size[1]
        cdef int index, kIndex
        for index in range(len(extrema)):
            hist = self.genHistogram(imData, extrema[index][0], extrema[index][1], totX, totY, 36, 0, 1)
            keys = hist.keys()
            for kIndex in range(len(keys)):
                pointDescriptor = self.getPointDescriptor(extrema[index][0], extrema[index][1], imData, totX, totY, keys[kIndex], hist[keys[kIndex]])
                if pointDescriptor != False and len(pointDescriptor) == 128:
                    descriptors.append(np.hstack(([extrema[index][0], extrema[index][1], hist[keys[kIndex]], keys[kIndex]], self.normalizeDescriptor(pointDescriptor))))
        return descriptors
    
    def getPointDescriptor(self, int pointX, int pointY, imagedata, int totX, int totY, float angle, float scale):
        """
        Extracts a descriptor from a given point in an image.
        The descriptor is expressed relatively to the initial angle.
        """
        cdef int x, y
        neighborhood = []
        for x in range(pointX-8, pointX+8, 4):
            if x <= 1 or x > totX-4:
                return 0
            for y in range(pointY-8, pointY+8, 4):
                if y <= 1 or y > totY-4:
                    return 0
                neighborhood.extend(self.genHistogram(imagedata, x+2, y+2, totX, totY, 8, angle, 0))
        return neighborhood
    
    def normalizeDescriptor(self, vector):
        """
        Normalizes a feature vector.
        Scales down outliers before normalization.
        """
        def normalize(v):
            soSquares = 0.0
            for el in vector:
                soSquares += el * el
            root = 1 + math.sqrt(soSquares)
            return [el/root for el in v]
        unitV = normalize(vector)
        return normalize([min(el, 0.2) for el in unitV])