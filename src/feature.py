import os, math, numpy, ImageFilter, Image
from cython import *
from histogram import *
'''Implementation of SIFT'''

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "siftify", os.sep, "data", os.sep)

class Sift(object):
    def __init__(self, sigma, numLevels = 1):
        self.sigma = sigma
        self.numOctaves = -1
        self.numLevels = numLevels

    def DoG(self, image):
        blendBeta = 8
        #pixelMin, pixelMax, pixelStd, pixelMean = imtools.pixelContrast(image)
        #magic = (pixelMean + (pixelMax - pixelMean)/2) / (2 * pixelStd)
        #blendBeta = blendBeta / (2 * magic)
        #print("=================")
        #print("Avg pixel: ", magic, pixelStd, blendBeta)
        #print("=================")
        self.numOctaves = int(math.log(min(image.size[0], image.size[1])))
        curImg = image.filter(ImageFilter.GaussianBlur(self.sigma))
        DoG = []
        scale = self.sigma
        for octave in range(self.numOctaves):
            tempImg = curImg.copy()
            diffs = []
            for level in range(-1, self.numLevels + 2):
                scale = self.sigma * math.pow(2, octave + level/self.numLevels)
                nextTemp= tempImg.filter(ImageFilter.GaussianBlur(scale))
                #nextTemp = Image.eval(tempImg, lambda x : self.gaus(x, scale * scale))
                diffs.append(Image.blend(tempImg, Image.eval(nextTemp, lambda x:x/blendBeta), 2.0).load())
                tempImg = nextTemp
            DoG.append(diffs)
        return DoG

    def getExtrema(self, image):
        '''get candidate interesting points'''
        DoGs = self.DoG(image)
        ret = []
        maxX, maxY = image.size
        for x in range(1, maxX - 1):
            for y in range(1, maxY - 1):
                isValid = True
                for level in range(self.numLevels):
                    if not self.isExtremum(DoGs[0][0][x,y], [DoGs[0][level][xy[0], xy[1]] for xy in self.genNeghbors(x, y)]):
                        isValid = False
                        break
                if isValid:
                    for octave in range(1, self.numOctaves):
                        if not isValid:break
                        for level in range(self.numLevels):
                            if not self.isExtremum(DoGs[0][0][x,y], [DoGs[octave][level][xy[0], xy[1]] for xy in self.genNeghbors(x, y)]):
                                isValid = False
                                break
                if isValid:ret.append((x,y))
        return ret

    def isExtremum(self, point, neighbors):
        '''Detect if max or min'''
        def up(p1, p2):
            return p1 > p2
        def down(p1, p2):
            return p1 < p2
        comp = {1:up, 0:down}
        dominates = comp[up(point, neighbors[0])]
        for i in range(len(neighbors)):
            if not dominates(point, neighbors[i]):
                return False
        return True

    def genNeghbors(self, xPos, yPos, considerSelf = False):
        '''
        Returns a list of neghboring pixels for (x, y).
        param considerSelf consideres pixel (x,y) as a valid neighbor of itself.
        '''
        ret = []
        for x in range(xPos-1, xPos+2):
            for y in filter(lambda yVal: considerSelf or yVal != yPos or x != xPos, range(yPos-1, yPos+2)):
                ret.append((x, y))
        return ret

    def gradientMagnitude(self, imData, point, parentDefault=1):
        '''Computes gradient magnitude for a given pixel in the image.'''
        (x,y) = point
        temp1 = imData[x+1, y] - imData[x-1, y]
        temp2 = imData[x, y+1] - imData[x, y-1]
        return math.sqrt((temp1 * temp1) + (temp2 * temp2)) * math.sqrt(parentDefault)

    def gradientOrientation(self, imData, point):
        '''Computes gradient orientation for a given pixel in the image.'''
        (x,y) = point
        temp1 = imData[x, y+1] - imData[x, y-1]
        temp2 = imData[x+1, y] - imData[x-1, y]
        return math.atan2(temp1, temp2)

    def genHistogram(self, imData, point, numBins, parentOrientation, filterHighest):
        '''Generates orientation histogram for a given point.'''
        histogram = Histogram(self.sigma, numBins)
        angle = abs(parentOrientation - self.gradientOrientation(imData, point))
        histogram.addToBins(angle, self.gradientMagnitude(imData, point))
        return histogram.getHighestBins(filterHighest)

    def getDescriptors(self, image):
        '''Generates image descriptors.'''
        #image = image.resize((image.size[0]/2, image.size[1]/2))
        extrema = self.getExtrema(image)
        descriptors = []
        imgData = image.load()
        imgSize = image.size
        for point in extrema:
            hist = self.genHistogram(imgData, point, 36, 0, True)
            for binKey in hist:
                pointDescriptor = self.getPointDescriptor(point, imgData, imgSize, binKey, hist[binKey])
                if pointDescriptor != False and len(pointDescriptor) == 128:
                    descriptors.append(numpy.hstack(([point[0], point[1], hist[binKey], binKey], self.normalizeDescriptor(pointDescriptor))))
        return descriptors

    def getPointDescriptor(self, point, imageData, imgSize, angle, scale):
        """
        Extracts a descriptor from a given point in an image.
        The descriptor is expressed relatively to the initial angle.
        """
        (totX, totY) = imgSize
        neighborhood = []
        for x in range(point[0]-8, point[0]+8, 4):
            if x <= 1 or x > totX-4:
                return False
            for y in range(point[1]-8, point[1]+8, 4):
                if y <= 1 or y > totY-4:
                    return False
                neighborhood.extend(self.genHistogram(imageData, (x+2,y+2), 8, angle, False))
        return neighborhood

    def normalizeDescriptor(self, vector):
        """
        Normalizes a feature vector.
        Scales down outliers before normalization.
        """
        def normalize(v):
            soSquares = 0.0
            for el in vector:
                soSquares += el * el
            root = 1 + math.sqrt(soSquares)
            return [el/root for el in v]
        unitV = normalize(vector)
        return normalize([min(el, 0.2) for el in unitV])

    def gaus(self, x, v):
        """
        Compute gaussian given x and the variance
        """
        return math.exp(-(x*x)/(2*v)) / math.sqrt(2 * v * math.pi)