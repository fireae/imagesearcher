import os, Image, pylab, numpy
import sift

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "imagesearcher", os.sep, "data", os.sep)

def plot_features(im, locs, circle=False):
    def draw_circle(c,r):
        t = numpy.arange(0,1.01,.01)*2*numpy.pi
        x = r*numpy.cos(t) + c[0]
        y = r*numpy.sin(t) + c[1]
        pylab.plot(x,y,'b',linewidth=2)

    pylab.imshow(im)
    if circle:
        for p in locs:
            draw_circle(p[:2],p[2])
    else:
        pylab.plot(locs[:,0], locs[:,1], 'ob')
    pylab.axis('off')

imname = DATA_PATH + 'images/A/8.png'
im1 = numpy.array(Image.open(imname).convert('L'))
sift.process_image(imname, DATA_PATH + 'temp.sift')
l1, d1 = sift.read_features_from_file(DATA_PATH + 'temp.sift')

print("FOUND ", d1.shape, " descr")
pylab.figure()
pylab.gray()
plot_features(im1, l1, True)
pylab.show()