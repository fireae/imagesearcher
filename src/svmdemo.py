import smo, kernel, knn, numpy as np, Image

def extractFeatVect(imgPath):
    img = Image.open(imgPath).convert("L").resize((32,32))
    w, h = img.size
    V = []
    for row in range(w):
        for col in range(h):
            V.append(img.getpixel((row, col)))
    return V
#data = [([1,1,1,1,1,1,1,1,1,1], 1), ([1,1,1,0,1,0,0,1,1,1], 1), ([1,1,1,0,1,1,1,0,1,0], 1), ([0,0,0,0,0,0,0,0,0,0], -1), ([0,0,0,0,0,0,0,0,1,1], -1)]
data = [([1,1], -1), ([0,1], 1), ([1,0], 1), ([0,0], -1)]

import imagesearch
import glob, os, pickle
os.chdir(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) + "/data/images")

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}".format(wDir, os.sep, "data", os.sep)



data = []
with open(DATA_PATH + 'vocab.pkl', 'rb') as f:
    voc = pickle.load(f)
searcher = imagesearch.Searcher(DATA_PATH + 'images.db', voc)

As =  [searcher.get_imhistogram(f) for f in glob.glob("A/*.png")]
Bs =  [searcher.get_imhistogram(f) for f in glob.glob("B/*.png")]
Cs =  [searcher.get_imhistogram(f) for f in glob.glob("C/*.png")]
Ds =  [searcher.get_imhistogram(f) for f in glob.glob("D/*.png")]
'''
As =  [extractFeatVect(f) for f in glob.glob("A/*.png")]
Bs =  [extractFeatVect(f) for f in glob.glob("B/*.png")]
Cs =  [extractFeatVect(f) for f in glob.glob("C/*.png")]
Ds =  [extractFeatVect(f) for f in glob.glob("D/*.png")]
Es =  [extractFeatVect(f) for f in glob.glob("E/*.png")]'''

for a in As:
    data.append((a, 1))
others = Bs
others.extend(Cs)
others.extend(Ds)
for other in others:
    data.append((other, -1))

dt = [(np.array(item[0]), item[1]) for item in data]
smoOpt = smo.SMO(np.array(dt), kernel.Kernel().rbf)

print("==============")
print("Support Vectors:")
'''for v in m.suportVectors:
    print(v.featureVector)'''
print("==============")
print("Support vectors:", len(smoOpt.sv), smoOpt.svYs, "b:", smoOpt.b)
print("ONES==============")
for point in As:
    print("CLASS", smoOpt.classify(point))
print("OTHERS============")
for point in others:
    print("CLASS", smoOpt.classify(point))
print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
#point = [1,0]
#print("CLASS", smoOpt.classify(point))
model = knn.KnnClassifier([item[1] for item in data], [item[0] for item in data])
for point in As:
    print("CLASS", model.classify(np.array(point)))
print("OTHERS============")
for point in others:
    print("CLASS", model.classify(np.array(point)))