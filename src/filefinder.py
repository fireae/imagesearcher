import Image, feature, imagesearch

DATA_PATH = "C:/Users/hakim/workspace/siftify/data/"

class FileFinder(object):
    def __init__(self, voc):
        self.voc = voc

    def getDescr(self, path):
        im = self.getImage(path)
        if im == None:
            return None
        descr = feature.Sift(2).getDescriptors(im)
        imname = path.rsplit(',', 1)
        indexer = imagesearch.Indexer(DATA_PATH + 'images.db', self.voc)
        indexer.add_to_index(self.randName(), descr)
        return descr
    
    def randName(self):
        import random
        import string
        char_set = string.ascii_uppercase + string.digits
        return ''.join(random.sample(char_set*6,6))