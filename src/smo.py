import random

class SMO(object):
    
    def __init__(self, data, kernel, epsilon=0.001, C=0.05, verbose=0):
        self.alpha = []
        self.y = [point[1] for point in data] #labels
        self.x = [point[0] for point in data]  #inputs
        self.sv = [point[0] for point in data] #support vectors
        self.w = [1 for point in data] #weights
        self.errors = {i:-self.y[i] for i in range(len(self.y))} #error cache
        self.epsilon = epsilon
        self.b = 0 # bias, or threshold
        self.C = C # regularization parameter
        self.kernel = kernel #kernel function
        self.verbose = verbose
        self.train()
        
    def classify(self, x):
        result = 0.0
        for idx in range(len(self.sv)):
            result += self.alpha[idx] * self.svYs[idx] * self.kernel(self.sv[idx], x)
        result += self.b
        if result >= 0:
            return 1.0
        return -1.0
    
    def dFunc(self, x):
        if self.verbose:
            print("alphas", self.alpha)
        result = 0.0
        for idx in range(len(self.y)):
            result += self.alpha[idx] * self.y[idx] * self.kernel(self.x[idx], x)
        result += self.b
        if self.verbose:
            print("f({}): {}".format(x, result))
        return result
    
    def calcAlpha2(self, i1, i2, L, H):
        '''
        Obtain a new value for alpha[i2]
        '''
        k = self.kernel
        k12 = k(self.x[i1], self.x[i2])
        k11 = k(self.x[i1], self.x[i1])
        k22 = k(self.x[i2], self.x[i2])
        eta = 2 * (k12 - k11 - k22)
        if eta < 0:
            a_new = self.alpha[i2] + (self.y[i2] * (self.getError(i1) - self.getError(i2)) / eta)
            if a_new < L:
                a_new = L
            elif a_new > H:
                a_new = H
        else:
            Lobj = self.objective(i1, i1, self.alpha[i1], L)
            Hobj = self.objective(i1, i1, self.alpha[i2], H)
            if Lobj > Hobj + self.epsilon:
                a_new = L
            elif Lobj < Hobj - self.epsilon:
                a_new = H
            else:
                a_new = self.alpha[i2]
        return a_new
    
    def objective(self, index1, index2, alpha1, alpha2):
        k = self.kernel
        k12 = k(self.x[index1], self.x[index2])
        k11 = k(self.x[index1], self.x[index1])
        k22 = k(self.x[index2], self.x[index2])
        eta = 2 * (k12 - k11 - k22)
        E1 = self.getError(index1)
        E2 = self.getError(index2)
        y2 = self.y[index2]
        res = (0.5 * eta * alpha2 * alpha2) + ((y2 * (E1 - E2)) - (eta * alpha2)) * alpha2 + self.C
        return res
        
    def getError(self, index):
        '''
        Check error.
        Uses cache for speed-up.
        '''
        if not index in self.errors:
            self.errors[index] = self.dFunc(self.x[index]) - self.y[index]
        return self.errors[index]
        
    def updateBias(self, index1, index2, a1_new, a2_new):
        '''
        Updates bias according to new alpha_1, and new alpha_2
        '''
        k = self.kernel
        k12 = k(self.x[index1], self.x[index2])
        k11 = k(self.x[index1], self.x[index1])
        k22 = k(self.x[index2], self.x[index2])
        err1 = self.y[index1] * (a1_new - self.alpha[index1])
        err2 = self.y[index2] * (a2_new - self.alpha[index2])
        if (self.alpha[index1] > 0) and (self.alpha[index1] < self.C):
            bnew = self.getError(index1) + (k11 * err1) + (k12 * err2) + self.b
        else:
            if (self.alpha[index2] > 0) and (self.alpha[index2] < self.C):
                bnew = self.getError(index1) + (k12 * err1) + (k22 * err2) + self.b
            else:
                b1 = self.getError(index1) + (k11 * err1) + (k12 * err2) + self.b
                b2 = self.getError(index1) + (k12 * err1) + (k22 * err2) + self.b
                bnew = (b1 + b2)/2
        diff = bnew - self.b
        self.b = bnew
        return diff
        
    def updateW(self, index1, index2, a1_new, a2_new):
        '''
        Update weight vector ( if linear SVM)
        '''
        if self.verbose:
            print("Index_1: {}; Index_2: {}; alpha_1: {}; alpha_2: {}".format(index1, index2, a1_new, a2_new))
    
    def updateErrorCache(self, index1, index2, a1_new, a2_new, deltaB):
        t1 = self.y[index1] * (self.alpha[index1] - a1_new)
        t2 = self.y[index2] * (self.alpha[index2] - a2_new)
        for i in range(len(self.alpha)):
            if (self.alpha[i] > 0) and (self.alpha[i] < self.C):
                self.errors[i] += (t1 * self.kernel(self.x[index1], self.x[i])) + (t2 * self.kernel(self.x[index2], self.x[i])) - deltaB
        self.errors[index1] = 0
        self.errors[index2] = 0
        
    def takeStep(self, i1, i2):
        '''
        attempt to update a chosen pair of Lagrange multipliers.
        param: i1, i2 are the indices of the parameters.
        '''
        if i1 == i2:
            return False
        #check for different bounds on alpha[i2]
        s = self.y[i1] * self.y[i2]
        #Compute L, H
        L = max(0, self.alpha[i2] + (s*self.alpha[i1]) - (self.C * (s + 1)/2))
        H = min(self.C, self.alpha[i2] + (s*self.alpha[i1]) - (self.C * (s - 1)/2))        
        if L == H:
            return 0
        #Obtain a new value for alpha[i2]
        alpha2_new = self.calcAlpha2(i1, i2, L, H)
        #Take care of numerical errors
        if alpha2_new < self.epsilon:
            alpha2_new = 0
        elif alpha2_new > self.C - self.epsilon:
            alpha2_new = self.C
        if abs(alpha2_new - self.alpha[i2]) < self.epsilon * (alpha2_new + self.alpha[i2] + self.epsilon):
            return 0
        #Update alpha_a1
        alpha1_new = self.alpha[i1] + s*(alpha2_new - self.alpha[i2])
        if alpha1_new < 0:
            alpha2_new += s * alpha1_new
            alpha1_new = 0
        elif alpha1_new > self.C:
            alpha2_new += s * (alpha1_new - self.C)
            alpha1_new = self.C
        deltaB = self.updateBias(i1, i2, alpha1_new, alpha2_new)
        self.updateW(i1, i2, alpha1_new, alpha2_new)
        self.updateErrorCache(i1, i2, alpha1_new, alpha2_new, deltaB)
        self.alpha[i1] = alpha1_new
        self.alpha[i2] = alpha2_new
        return True
        
    def examineExample(self, i1):
        y1 = self.y[i1]
        alpha1 = self.alpha[i1]
        if (alpha1 > 0 and alpha1 < self.C):
            E1 = self.getError(i1)
        else:
            E1 = self.dFunc(self.x[i1]) - self.y[i1]
        r1 = y1 * E1
        if (r1 < -self.epsilon and alpha1 < self.C) or (r1 > self.epsilon and alpha1 > 0):
            if self.tryErrMax(i1, E1):
                return 1
            elif self.chooseAlpha2(i1):
                return 1
        return 0
    
    def tryErrMax(self, i1, E1):
        i2 = -1
        tmax = 0
        for k in range(len(self.alpha)):
            if self.alpha[k] > 0 and self.alpha[k] < self.C:
                E2 = self.getError(k)
                temp = abs(E1 - E2)
                if temp > tmax:
                    tmax = temp
                    i2 = k
        if i2 >= 0:
            if self.takeStep(i1, i2):
                return True
        return False
    
    def chooseAlpha2(self, i1):
        #Iterate through non-boundary examples
        nonBoundaryAlphas = [i for i in range(len(self.alpha)) if (self.alpha[i] != 0 and self.alpha[i] != self.C)]
        randOffset = random.randint(0, len(nonBoundaryAlphas))
        totLen = len(nonBoundaryAlphas)
        for idx in range(randOffset, totLen + randOffset):
            i2 = nonBoundaryAlphas[idx % totLen]
            if (self.alpha[i2] > 0) and (self.alpha[i2] < self.C):
                if self.takeStep(i1, i2):
                    return True
        #Iterate through entire training set
        randOffset = random.randint(0, totLen + randOffset)
        totLen = len(self.alpha)
        for idx in range(randOffset, len(self.alpha)):
            i2 = idx % totLen
            if (self.alpha[i2] > 0) and (self.alpha[i2] < self.C):
                if self.takeStep(i1, i2):
                    return True
        return False
        
    def train(self):
        self.alpha = [0.04 for i in range(len(self.x))]
        self.b = 0
        numChanged = 0
        examineAll = True
        while (numChanged > 0 or examineAll):
            #print("BIAS: {}; ALPHAS: {}; changed: {}".format(self.b, self.alpha, numChanged))
            numChanged = 0
            if examineAll:
                for idx in range(len(self.x)):
                    numChanged += self.examineExample(idx)
            else:
                validAlphaIndexes = [i for i in range(len(self.alpha)) if (self.alpha[i] != 0 and self.alpha[i] != self.C)]
                for alpha in validAlphaIndexes:
                    numChanged += self.examineExample(alpha)
            if examineAll:
                examineAll = False
            elif numChanged == 0:
                examineAll = True     
        #print("BIAS: {}; ALPHAS: {}; changed: {}".format(self.b, self.alpha, numChanged))
        self.sv = [self.x[i] for i in range(len(self.alpha)) if (self.alpha[i] > 0 and self.alpha[i] < self.C + self.epsilon)]
        self.svYs = [self.y[i] for i in range(len(self.alpha)) if (self.alpha[i] > 0 and self.alpha[i] < self.C + self.epsilon)]
        self.w = [self.w[i] for i in range(len(self.alpha)) if (self.alpha[i] > 0 and self.alpha[i] < self.C + self.epsilon)]
                