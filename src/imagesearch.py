import pickle, math
import sqlite3 as sqlite

class Indexer(object):

    def __init__(self, db, voc):
        self.con = sqlite.connect(db)
        self.voc = voc

    def __del__(self):
        self.con.close()

    def db_commit(self):
        self.con.commit()

    def create_tables(self):
        self.con.execute('create table if not exists imlist(filename)')
        self.con.execute('create table if not exists imwords(imid, wordid, vocname)')
        self.con.execute('create table if not exists imhistograms(imid, histogram, vocname)')
        self.con.execute('create index if not exists im_idx on imlist(filename)')
        self.con.execute('create index if not exists word_idx on imwords(wordid)')
        self.con.execute('create index if not exists imid_idx on imwords(imid)')
        self.con.execute('create index if not exists imidhist_idx on imhistograms(imid)')
        self.db_commit()

    def add_to_index(self, imname,  descr):
        """ Take an image with feature descriptors,
        project on vocabulary and add to database. """

        if self.is_indexed(imname):return
        #get the imid
        imid = self.get_id(imname)
        #get the words
        imwords = self.voc.project(descr)
        nbr_words = imwords.shape[0]

        #link each word to image
        for i in range(nbr_words):
            word = imwords[i]
            self.con.execute("insert into imwords(imid, wordid, vocname) values (?,?,?)", (imid, word, self.voc.name))
        #store word histogram for image
        #use pickle to encode Numpy array as strings
        self.con.execute("insert into imhistograms(imid,histogram,vocname) values(?,?,?)", (imid, pickle.dumps(imwords), self.voc.name))

    def is_indexed(self, imname):
        im = self.con.execute("select rowid from imlist where filename='%s'" % imname).fetchone()
        return im != None

    def get_id(self,imname):
        """ Get an entry id and add it if not present. """
        cur = self.con.execute("select rowid from imlist where filename='%s'" %imname)
        res = cur.fetchone()
        if res == None:
            cur = self.con.execute("insert into imlist(filename) values('%s')" %imname)
            return cur.lastrowid
        else:
            return res[0]

class Searcher(object):

    def __init__(self, db, voc):
        self.con = sqlite.connect(db)
        self.voc = voc

    def __del__(self):
        self.con.close()

    def gen_histogram_from_image(self, impath):
        import feature, Image, numpy
        image = Image.open(impath).convert('L')
        descr = numpy.array(feature.Sift(0.7071).getDescriptors(image))[:,4:]
        return self.voc.project(descr)

    def candidates_from_word(self, imword):
        """ Get list of images containing imwords. """
        im_ids = self.con.execute("select distinct imid from imwords where wordid=%d" %imword).fetchall()
        return [i[0] for i in im_ids]

    def candidates_from_histogram(self, imwords):
        """ Get list of images with similar words. """
        #get word ids
        words = imwords.nonzero()[0]
        #find candidates
        candidates = []
        for word in words:
            c = self.candidates_from_word(word)
            candidates += c

        #take all unique words amd reverse sort on occurence
        tmp = [(w, candidates.count(w)) for w in set(candidates)]
        tmp.sort(cmp=lambda x,y:cmp(x[1],y[1]))
        tmp.reverse()
        #return sorted list, best matches first
        return [w[0] for w in tmp]

    def get_imhistogram(self, imname):
        """ Return the word histogram for an image. """
        im_id = self.con.execute("select rowid from imlist where filename='%s'" %imname).fetchone()
        s = self.con.execute("select histogram from imhistograms where rowid='%d'" %im_id).fetchone()
        #use pickle to decode Numpy arrays from string
        return pickle.loads(str(s[0]))

    def query(self, imname, indexed):
        """ Find a list of matching images for imname. """

        if indexed: h = self.get_imhistogram(imname)
        else: h = self.gen_histogram_from_image(imname)
        candidates = self.candidates_from_histogram(h)
        matchscores = []
        for imid in candidates:
            #get the name
            cand_name = self.con.execute("select filename from imlist where rowid=%d" % imid).fetchone()
            cand_h = self.get_imhistogram(cand_name)
            try:
                cand_dist = math.sqrt( sum( (h - cand_h)**2 ) ) #use L2 distance
                matchscores.append((cand_dist, imid))
            except ValueError:
                pass
        #return a sorted list of distances and database ids
        matchscores.sort()
        return matchscores

    def get_filename(self, imid):
        s = self.con.execute("select filename from imlist where rowid='%d'" %imid).fetchone()
        return s[0]