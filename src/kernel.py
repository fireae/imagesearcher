import math, operator

class Kernel(object):
    LINEAR = 1
    POLYNOMIAL = 2
    RBF = 3
    SIGMOID = 4
    def __init__(self, degree=3, linCoef=20, constCoef=15, gamma=0.45):
        '''
        Initializes kernel params:
        -degree : necessary for polynomial kernels
        -linCoef : necessary for polynomial & sigmoid kernels
        -constCoef: necessary for polynomial & sigmoid kernels
        -gamma: necessary for RBF(gaussian) kernels
        '''
        self.gamma = gamma
        self.degree = degree
        self.linCoef = linCoef
        self.constCoef = constCoef
    
    def linear(self, vec1, vec2):
        '''
        Returns inner product of two vectors.
        '''
        return sum(map(operator.mul, vec1, vec2))
    
    def poly(self, vec1, vec2):
        return math.pow((self.linCoef * self.linear(vec1, vec2)) + self.constCoef, self.degree)
    
    def sigmoid(self, vec1, vec2):
        return math.tanh((self.linCoef * self.linear(vec1, vec2)) + self.constCoef)
    
    def rbf(self, vec1, vec2):
        return math.exp(-self.gamma * abs(self.linear((vec1 - vec2), (vec1 - vec2))))
    
    def logistic(self, vec1, vec2):
        x = self.linear(vec1, vec2) + self.constCoef
        return self.linCoef * x / math.sqrt(abs(1 + x))
    
    def kernel(self, vec1, vec2):
        return self.linear(vec1, vec2)
