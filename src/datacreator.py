from numpy.random import randn
from numpy import *
import os, pickle

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}{}{}".format(wDir, os.sep, "images", os.sep, "data", os.sep, "points", os.sep)

#create sample data of 2D points
n = 200

# two normal distributions
class_1 = 0.6 * randn(n, 2)
class_2 = 1.2 * randn(n, 2) + array([5,1])
labels = hstack((ones(n), -ones(n)))

with open(DATA_PATH + 'points_normal.pkl', 'w') as f:
    pickle.dump(class_1, f) 
    pickle.dump(class_2, f)
    pickle.dump(labels, f)
    f.close()
    
# normal distribution and ring around it
class_1 = 0.6 * randn(n, 2)
r = 0.8 * randn(n,1) + 5
angle = 2*pi * randn(n,1)
class_2 = hstack((r*cos(angle), r*sin(angle)))
labels = hstack((ones(n), -ones(n)))

with open(DATA_PATH + 'points_ring.pkl', 'w') as f:
    pickle.dump(class_1, f) 
    pickle.dump(class_2, f)
    pickle.dump(labels, f)
    f.close()