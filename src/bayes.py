import numpy, math

class Bayes(object):
    
    def __init__(self):
        self.labels = []
        self.mean = []
        self.var = [] #variances
        self.n = 0 #n of classes
        
    def train(self, data, labels=None):
        if labels == None:
            labels = range(len(data))
        self.labels = labels
        self.n = len(labels)
        for c in data:
            self.mean.append(numpy.mean(c, 0))
            self.var.append(numpy.var(c,0))
            
    def classify(self, points):
        est_prob = numpy.array([self.gauss(m,v,numpy.array(points)) for m,v in zip(self.mean,self.var)])
        ndx = est_prob.argmax(axis=0)
        est_labels = numpy.array([self.labels[n] for n in ndx])
        return est_labels, est_prob
    
    def gauss(self,m,v,x):
        if len(x.shape) == 1:
            n,d = 1,x.shape[0]
        else:
            n,d = x.shape
        S = numpy.diag(1/v)
        x = x-m
        y = math.exp(-0.5*numpy.diag(numpy.dot(S, x.T)))
        return y * (2*math.pi)**(-d/2.0) / (math.sqrt(numpy.prod(v)) + 1e-6)