import os, pickle, time, cProfile
import sift as sift
import imagesearch, vocabulary

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "imagesearcher", os.sep, "data", os.sep)
IMAGES_PATH = "{}{}{}".format(DATA_PATH, "images", os.sep)
import glob
os.chdir(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) + "/data/images/")
'''for files in glob.glob("*.jpg"):
    print files
'''
imlist = [f for f in glob.glob("*/*.png")]
#imlist = imlist[0::5]
imlist = imlist[0::5] + imlist[1::10]
featlist = [im[:-4] + ".sift" for im in imlist]

def testSearch():
    ndx = 0
    voc = vocabulary.openVoc('../vocab.pkl')
    t = time.time()
    #voc.train([IMAGES_PATH + featfile for featfile in featlist], 1000, 1)
    locs, descr = sift.read_features_from_file(IMAGES_PATH + featlist[0])
    print("=====")
    voc.project(descr)
    src = imagesearch.Searcher('../images.db', voc)
    print("Test Query.... for image {}.".format(imlist[ndx]))
    result = src.query(imlist[ndx], False)[:20]
    print([(res[0], src.get_filename(res[1])) for res in result])
    t = time.time() - t
    print("Tot time: ", t, "seconds")

def processImages():
    nbr_images = len(imlist)
    for i in range(nbr_images):
        sift.process_image(IMAGES_PATH + imlist[i], IMAGES_PATH + featlist[i])

def indexImages():
    t = time.time()
    nbr_images = len(imlist)
    #processImages()
    vocabulary.createVoc([IMAGES_PATH + imName for imName in imlist], DATA_PATH + 'vocab.pkl')
    #load voc
    with open(DATA_PATH + 'vocab.pkl', 'rb') as f:
        voc = pickle.load(f)
    #create indexer
    indx = imagesearch.Indexer(DATA_PATH + 'images.db', voc)
    indx.create_tables()

    #go through all images, project features on vocabulary and insert
    for i in range(nbr_images):
        try:
            locs, descr = sift.read_features_from_file(IMAGES_PATH + featlist[i])
            indx.add_to_index(imlist[i], descr)
        except:
            pass
    indx.db_commit()
    t = time.time() - t
    print("FINALLY!!!", t, "seconds")

def test():
    import feature as cfeature, Image
    image = Image.open(DATA_PATH + "/images/CUP/8.png").convert("L")
    a = cfeature.Sift(0.7071).getDescriptors(image)
    print(len(a))

#test()
#testSearch()
processImages()
#indexImages()
#cProfile.run('testSearch()')

'''
import numpy, vectorutils
from scipy.cluster.vq import *
code_book = numpy.array([[1.,1.,1.],[2.,2.,2.]])
features  = numpy.array([[  1.9,2.3,1.7],[  0.8,0.6,1.7],[  1.5,2.5,2.2], [  1.9,2.5,5.2], [  4.5,7.5,2.2], [  1.3,4.5,5.2], [  2.5,7.5,2.2], [  5.5,3.5,1.2]])
#print(vq(features,code_book))
codes, dist = kmeans(whiten(features), 2, 1)
print("SCIPY: ", codes, "=>", dist)

c, d = vectorutils.kmeans(whiten(features), 2, 3)
print("ME: ", c, "=>", d)'''