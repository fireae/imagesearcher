import os, pickle
import knn, bayes, smo, kernel
import numpy as np, pylab

def plot_2D_boundary(plot_range, points, decisionfcn, labels, model, values=[0]):
    """
    Plot_range is (xmin, xmax,ymin,ymax), points is a list of class points,
    decisionfcn is a function to evaluate,
    labels is a list of labels that decisionfcn returns for each class,
    values is a list of decision contours to show.
    """

    clist = ['b','y','r','g','k','m'] #colors for the classes

    #evaluate on a grid and plot contour of decision function
    x = np.arange(plot_range[0], plot_range[1],.1)
    y = np.arange(plot_range[2], plot_range[3],.1)
    xx,yy = np.meshgrid(x,y)
    xxx,yyy = xx.flatten(), yy.flatten() #lists of x,y in grid
    zz = np.array(decisionfcn(xxx,yyy, model))
    zz = zz.reshape(xx.shape)
    #plot contour(s) at values
    pylab.contour(xx,yy,zz,values)
    #for each class, plot the points with '*' for correct, 'o' for incorrect
    for i in range(len(points)):
        d = decisionfcn(points[i][:,0], points[i][:,1], model)
        correct_ndx = labels[i] == d
        incorrect_ndx = labels[i] != d
        pylab.plot(points[i][correct_ndx,0], points[i][correct_ndx,1], '*', color=clist[i])
        pylab.plot(points[i][incorrect_ndx,0], points[i][incorrect_ndx,1], 'o', color=clist[i])
    pylab.axis('equal')

def classify(x,y,model):
    return np.array([model.classify([xx,yy]) for (xx,yy) in zip(x,y)])


wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "data", os.sep, "points", os.sep)

with open(DATA_PATH + 'points_normal.pkl', 'r') as f:
    class_1 = pickle.load(f)
    class_2 = pickle.load(f)
    labels = pickle.load(f)
    f.close()

model = knn.KnnClassifier(labels, np.vstack((class_1, class_2)))
data = np.concatenate((class_1, class_2))
data = zip(data, labels)
model = smo.SMO(data, kernel.Kernel().rbf)
print(len(model.sv), model.svYs)
print("b", model.b)
plot_range = [-3,3,-3,3]
plot_2D_boundary(plot_range, [class_1,class_2], classify, [1,-1], model)
pylab.show()