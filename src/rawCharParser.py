import os, pickle, glob, Image, time, numpy as np

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}{}{}".format(wDir, os.sep, "images", os.sep, "data", os.sep)
RAW_CHARS_PATH = "{}{}{}".format(DATA_PATH, "rawchars", os.sep)
CHAR_IMAGES_PATH = "{}{}{}".format(DATA_PATH, "charimages", os.sep)

os.chdir(RAW_CHARS_PATH)
files = [RAW_CHARS_PATH + f for f in glob.glob("*")]
os.chdir(CHAR_IMAGES_PATH)

def processChar(character):
    oc = character.find("|")
    if oc < 0:
        return False
    oc = character.find("|", oc+1)
    x,y = map(int, character[:oc].split("|"))
    def trans(x):
        if x == "0":return 255
        return 0
    rows = [map(trans, list(row)) for row in character[oc+1:].replace("O", "0").replace("X", "1").strip().split("|")]
    rawChar = np.array(filter(lambda k: len(k) == x, rows))
    img = Image.fromarray(rawChar, "L")
    return img

def saveImg(img, dirName):
    if img != False:
        if not os.path.isdir(dirName):
            os.makedirs(dirName)
        img.save(dirName + os.sep + dirName + str(time.time()) + ".jpg")
        
def extractCharsInFile(path):
    with open(path) as f:
        lines = f.readlines()
        for line in lines:
            sep = line.find(":")
            symbol = line[:sep] 
            data = line[sep+1:].split(":")
            for character in data:
                saveImg(processChar(character), symbol)


if __name__ == "__main__":
    extractCharsInFile(files[0])