import Image
from numpy import loadtxt, savetxt, linalg, hstack, zeros, argsort, dot, arccos, array
import os, feature#cfeature as feature

def process_image(imagename, resultname):
    """ Process an image and save the results in a file. """
    im = Image.open(imagename).convert('L')
    result = feature.Sift(0.10).getDescriptors(im)
    with open(resultname, 'w') as f:
        savetxt(f, result)
        f.close()

def sys_process_image(imagename, resultname):
    """ Process an image and save the results in a file. """
    im = Image.open(imagename).convert('L')
    im.save('tmp.pgm')
    imagename = 'tmp.pgm'
    params = "--edge-thresh 1000 --peak-thresh 0"
    cmmd = str("C:\\Users\\hakim\\Desktop\\vlfeat\\vlfeat-0.9.17\\bin\\win32\\sift " + imagename + " --output=" + resultname + " " + params)
    os.system(cmmd)

def read_features_from_file(filename):
    """ Read feature properties and return in matrix form. """
    f = loadtxt(filename)
    print("processing ", filename)
    locs = f[:,:4]
    descr = f[:,4:]
    #V, S, m = pca.pca(descr)
    #descr = array([dot(V, f-m) for f in descr])
    return  locs, descr#feature locations, descriptors


def write_features_to_file(filename, locs, desc):
    """ Save feature location and descriptor to file. """
    savetxt(filename, hstack((locs, desc)))

def match(desc1, desc2):
    """ For each descriptor in the first image,
    select its match in the second image.
    input: desc1 (description for the first image),
    desc2 (same for second image). """
    desc1 = array([d/linalg.norm(d) for d in desc1])
    desc2 = array([d/linalg.norm(d) for d in desc2])

    dist_ratio = 0.6
    desc1_size = desc1.shape

    matchscores = zeros((desc1_size[0], 1), 'int')
    desc2t = desc2.T #precompute matrix transpose
    for i in range(desc1_size[0]):
        dotprods = dot(desc1[i,:], desc2t) #vector of dot products
        dotprods = 0.9999*dotprods
        #inverse cosine and sort, return index for features in second image
        indx = argsort(arccos(dotprods))

    #check if nearest neighbor has angle less than dist_ratio times 2nd
    if arccos(dotprods)[indx[0]] < dist_ratio * arccos(dotprods)[indx[1]]:
        matchscores[i] = int(indx[0])

    return matchscores

def match_two_sided(desc1, desc2):
    """ Two-sided symmetric version of mathc(). """
    matches_12 = match(desc1, desc2)
    matches_21 = match(desc2, desc1)

    ndx_12 = matches_12.nonzero()[0]

    #remove matches that are not symmetric
    for n in ndx_12:
        if matches_21[int(matches_12[n])] != n:
            matches_12[n] = 0

    return matches_12