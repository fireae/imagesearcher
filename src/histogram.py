import math

class Histogram(object):
    def __init__(self, sigma, numBins):
        assert numBins > 0
        self.sigma = sigma
        self.bins = {}
        self.binAngle = int(360 / numBins)
        for binKey in range(0, 360, self.binAngle):
            self.bins[binKey] = 0.0
            
    def addToBins(self, orientation, magnitude):
        binKey = self.binAngle * round(1.0 * orientation / self.binAngle)
        oldVal = self.bins[binKey%360]
        update = oldVal + self.gaussian(magnitude, 0, 1.5 * self.sigma)
        self.bins[binKey] = update
    
    def getHighestBins(self, filterHighest):
        bins = sorted(self.bins, key=self.bins.get, reverse=True)
        if not filterHighest:
            return bins
        highest = bins[0]
        threshold = 0.8 * self.bins.get(highest)
        ret = {highest: self.bins.get(highest)}
        for binVal in bins[1:]:
            val = self.bins.get(binVal)
            if  val >= threshold:
                ret[binVal] = val
            else:break      
        return ret
    
    def gaussian(self, x, y, sigma):
        sigmaSq = sigma * sigma
        denom = 2 * math.pi * sigmaSq
        rootExp = -((x*x) + (y*y))/(2*sigmaSq)
        return math.exp(rootExp)/denom