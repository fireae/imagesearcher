import Tkinter as tk, tkFileDialog as tFile, ttk
import os, pickle, random, threading
import imagesearch
import glob
import ImageTk, Image
os.chdir(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) + "/data/images")

wDir = os.path.dirname(os.path.abspath(os.path.join(os.pardir)))
DATA_PATH = "{}{}{}{}".format(wDir, os.sep, "data", os.sep)

class Fetcher(threading.Thread):
    def __init__(self, parent, idd, name, imname, progWidget):
        threading.Thread.__init__(self)
        self.threadID = idd
        self.name = name
        self.imname = imname
        self.progWidget = progWidget
        self.parent = parent

    def run(self):
        self.parent.fetch(self.imname, self.progWidget)

class DemoUI(tk.Frame):
    def __init__(self, w, h, master=None):
        tk.Frame.__init__(self, master)
        self.imlist = [f for f in glob.glob("*/*.png")]
        self.nbr_images = len(self.imlist)
        with open(DATA_PATH + 'vocab.pkl', 'rb') as f:
            self.voc = pickle.load(f)
        self.maxres = 20
        self.maxmatches = 10
        self.imSize = 100
        self.sections = (0.7, 0.28)
        self.master.title("Demo UI")
        self.master.minsize(w, h)
        leftPane = tk.PanedWindow(self, width=int(w * self.sections[0]), bg="cyan")
        buttonsPane = tk.PanedWindow(leftPane, width=int(w * self.sections[0]))
        self.choicePane = tk.PanedWindow(leftPane, width=leftPane.size()[0])
        vsep = tk.PanedWindow(self, width=1, height=h, bg="black")
        hsep = tk.PanedWindow(leftPane, width=int(w * self.sections[0]), height=5, bg="black")
        self.evalPane = tk.PanedWindow(self, width=int(w * self.sections[1]))
        reloadButton = tk.Button(buttonsPane, text="Reload From Index DB", command=self.refresh, width=25, height=5, bg="red")
        testButton = tk.Button(buttonsPane, text="Open New Test", command=self.explore, width=25, height=5, bg="blue")
        self.choicePane.grid(row = 0, column = 0)
        reloadButton.grid(row=1, column=0, padx=(10,10), pady=(10,10))
        testButton.grid(row=1, column=2, padx=(10,10), pady=(10,10))
        vsep.grid(row = 0, column = 1, padx=(5,5))
        self.evalPane.grid(row = 0, column = 2)
        leftPane.grid(row=0, column=0, pady=(20,20))
        hsep.grid(row = 1, column = 0, pady=(20,20))
        buttonsPane.grid(row=2, column=0, pady=(10,10))
        self.width, self.height = w, h
        self.pack()

    def fetch(self, im, progrWidget):
        self.searcher = imagesearch.Searcher(DATA_PATH + 'images.db', self.voc)
        self.indexer = imagesearch.Indexer(DATA_PATH + 'images.db', self.voc)
        res = self.searcher.query(im, self.indexer.is_indexed(im))[:self.maxmatches]
        if res:
            progrWidget.destroy()
            labels = []
            for dist, ndx in res:
                labels.append(self.searcher.get_filename(ndx))
            cols = int(self.width * self.sections[1]) / (self.imSize + 5)
            rows = len(labels) / cols
            for i in range(rows):
                for j in range(cols):
                    img = Image.open(labels[cols * i + j])
                    img = img.resize((self.imSize, self.imSize), Image.ANTIALIAS)
                    im = ImageTk.PhotoImage(img)
                    label = tk.Label(self.evalPane, image = im, width = self.imSize, height = self.imSize)
                    label.image = im
                    label.grid(row = i+1, column = j, pady = (5,5), padx = (5,5))
        else:
            label = tk.Label(self.evalPane, text="No results found!")
            label.grid(row=2, column = 2, padx=(10,10), pady=(10,10))

    def query(self, im):
        if im:
            for child in self.evalPane.winfo_children():
                child.destroy()
            progr = ttk.Progressbar(self.evalPane, orient=tk.HORIZONTAL, length=200, mode='indeterminate', maximum=100)
            progr.grid(row=0, column=1)
            progr.start(20)
            t = Fetcher(self, 1, "name", im, progr)
            t.start()
            t.join()
        else:
            self.reload(self.imlist)
        self.pack()

    def reload(self, imlist, parentDir=None):
        for child in self.evalPane.winfo_children():
            child.grid_forget()
        random.shuffle(imlist)
        cols = int(self.width * self.sections[0] / (self.imSize + 5))
        rows = min(self.maxres, len(imlist)) / cols
        for i in range(rows):
            for j in range(cols):
                imid = imlist[cols * i + j]
                img = Image.open(imid)
                img = img.resize((self.imSize, self.imSize), Image.ANTIALIAS)
                im = ImageTk.PhotoImage(img)
                label = tk.Button(self.choicePane, image = im, width = self.imSize, height = self.imSize, command = lambda x=imid: self.query(x))
                label.image = im
                label.grid(row = i, column = j, pady = (5,5), padx = (5,5))
        self.pack()

    def refresh(self):
        self.reload(self.imlist)

    def explore(self):
        dirName = tFile.askdirectory()
        if dirName:
            imlist = [f for f in glob.glob(dirName + os.sep +"*.png")]
            self.reload(imlist, dirName)

app = DemoUI(1200, 700)
app.mainloop()
