ImageSearcher is a simple image indexing and retrieval application built in python.
It uses SIFT and FAST for feature extraction.
Learning models used for classification are: naive Bayes, Support Vector Machines, K-Nearest Neighbors, and Self-Organizing Maps.

ENVIRONMENT
-----------
python 2.6

DEPENDENCIES
------------
1. sqlite3
2. scipy


### Running ###
* Prepare a directory containing images for training
* Prepare a directory containing images for testing
* Run demoUI.py (src/demoUI.py) to bring up the user interface
* Select which model to use for tests (naive Bayes, KNN, SVM, or SOM)